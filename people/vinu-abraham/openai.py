#!/usr/bin/env python
# coding: utf-8

# In[1]:


import gym
import random
import numpy as np


# In[2]:


env_name = "GuessingGame-v0"
env = gym.make(env_name)


# In[3]:


class Agent:
    def __init__(self, env):  # self.action_size = env.action_space.n
        self.action_low = env.action_space.low
        self.action_high = env.action_space.high
        self.action_shape = env.action_space.shape

    def get_action(self, state):

        action = np.random.uniform(self.action_low, self.action_high, self.action_shape)
        return action


# In[5]:


agent = Agent(env)
state = env.reset()
for i in range(100):
    action = agent.get_action(state)
    state, reward, done, info = env.step(action)
    # print("state:",state,"done:",done,"i is",i)
    # env.render()


# In[ ]:
